package com.quocdao.demo.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

public class MQFilter implements Filter {

    public MQFilter() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        HttpServletRequest request = (HttpServletRequest) req;

        Enumeration<String> headerNames = request.getHeaderNames();
        if (headerNames != null) {
            if (!"mq".equals(request.getHeader("Authorization"))) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You don't have valid api key!");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
        }

        chain.doFilter(req, res);
    }
}
