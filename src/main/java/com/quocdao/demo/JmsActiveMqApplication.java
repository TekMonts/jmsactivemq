package com.quocdao.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
public class JmsActiveMqApplication {

	public static void main(String[] args) {
		SpringApplication.run(JmsActiveMqApplication.class, args);
	}

}
