package com.quocdao.demo.controller;

import com.quocdao.demo.mq.Producer;
import com.quocdao.demo.utility.common.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/message")
public class MQController {
    @Autowired
    private Producer producer;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ResponseData<Object>> postMessage(@RequestBody String message) throws Exception {
        producer.sendMessage("cap.jms.demo.inbound.queue", message);
        ResponseData<Object> responseData = ResponseData.builder().msgContent(message)
                .httpStatus(HttpStatus.OK).build();
        return ResponseEntity.status(responseData.getHttpStatus()).body(responseData);
    }
}
