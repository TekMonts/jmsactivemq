package com.quocdao.demo.utility.common;

import lombok.*;
import org.springframework.http.HttpStatus;

@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResponseData<T> {
    private T data;
    private Integer msgId;
    private String msgContent;
    private HttpStatus httpStatus;
}
