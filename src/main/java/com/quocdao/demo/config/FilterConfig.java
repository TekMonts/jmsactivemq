package com.quocdao.demo.config;

import com.quocdao.demo.filter.MQFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {
    @Bean
    public FilterRegistrationBean<MQFilter> productFilter(){
        FilterRegistrationBean<MQFilter> registrationBean
                = new FilterRegistrationBean<>();
        registrationBean.setFilter(new MQFilter());
        registrationBean.addUrlPatterns("/message/*");
        return registrationBean;
    }
}
