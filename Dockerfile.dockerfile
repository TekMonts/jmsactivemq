FROM adoptopenjdk/openjdk8:alpine-jre as builder

ARG JAR_FILE=target/jmsActiveMQDemo-0.0.1-SNAPSHOT.jar

# cd /opt/app
WORKDIR /opt/app

# cp target/spring-boot-web.jar /opt/app/app.jar
COPY ${JAR_FILE} appjmsfilter.jar

# java -jar /opt/app/app.jar
ENTRYPOINT ["java","-jar","appjmsfilter.jar"]