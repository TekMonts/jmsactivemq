FROM adoptopenjdk/openjdk8:alpine-jre
COPY target/jmsActiveMQDemo-0.0.1-SNAPSHOT.jar appjmsfilter.jar
CMD ["java","-jar","appjmsfilter.jar"]